FROM archlinux:base
LABEL maintainer="Enmanuel Moreira"
ENV container docker

# Source: https://github.com/CarloDePieri/docker-archlinux-systemd/blob/master/Dockerfile
# Source: https://github.com/CarloDePieri/docker-archlinux-ansible/blob/master/Dockerfile

# Source: https://github.com/qutebrowser/qutebrowser/commit/478e4de7bd1f26bebdcdc166d5369b2b5142c3e2
# WORKAROUND for glibc 2.33 and old Docker
# See https://github.com/actions/virtual-environments/issues/2658
# Thanks to https://github.com/lxqt/lxqt-panel/pull/1562
# Remove this once upstream archlinux:latest image is fixed
#RUN patched_glibc=glibc-linux4-2.33-4-x86_64.pkg.tar.zst \
#  && curl -LO "https://repo.archlinuxcn.org/x86_64/$patched_glibc" \
#  && bsdtar -C / -xvf "$patched_glibc"

SHELL ["/bin/bash", "-eo", "pipefail", "-c"]
# hadolint ignore=DL3003
RUN set -ex \
  && pacman-key --init \
  && pacman-key --refresh-keys \
  && pacman -Syu --noconfirm --needed \
  python ansible \
  sudo \
  && pacman -Scc --noconfirm \
  && (cd /lib/systemd/system/sysinit.target.wants/; \
  for i in *; do [ "$i" == systemd-tmpfiles-setup.service ] || rm -f "$i"; done); \
  rm -f /lib/systemd/system/multi-user.target.wants/*; \
  rm -f /etc/systemd/system/*.wants/*; \
  rm -f /lib/systemd/system/local-fs.target.wants/*; \
  rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
  rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
  rm -f /lib/systemd/system/basic.target.wants/*; \
  rm -f /lib/systemd/system/anaconda.target.wants/*; \
  mkdir -p /etc/ansible \
  && printf '[local]\nlocalhost ansible_connection=local' > /etc/ansible/hosts \
  && sed -i -e 's/^\(Defaults\s*requiretty\)/#--- \1/'  /etc/sudoers \
  && groupadd -r ansible \
  && useradd -m ansible -p ansible -g ansible \
  && usermod -aG wheel ansible \
  && sed -i "s/# %wheel ALL=(ALL) NOPASSWD: ALL/%wheel ALL=(ALL) NOPASSWD: ALL/g" /etc/sudoers

VOLUME ["/sys/fs/cgroup"]

CMD ["/usr/sbin/init"]
